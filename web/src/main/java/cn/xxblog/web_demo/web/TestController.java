package cn.xxblog.web_demo.web;
import cn.xxblog.cache.cn.xxblog.cache.distribute.RedisLock;
import cn.xxblog.cache.cn.xxblog.cache.redis.RedisCache;
import cn.xxblog.web_demo.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author barryp
 * @create 2018-07-06 10:46
 *     description:
 */
@RestController
@Slf4j
@RequestMapping(value = "/api/")
public class TestController {

    @Autowired
    private TestService testService;


    @RequestMapping(value = "test")
    public void test() {
        log.info("test start");
        RedisLock redisLock = new RedisLock("default", "lock_test", 50000000);
        if (redisLock.tryLock(5000)) {
            System.out.println("1111111");
            // redisLock.unlock();
        }
        testService.test();
        log.info("test end");
    }


    @RequestMapping(value = "test1")
    public void test1() {
        RedisLock redisLock = new RedisLock("default", "lock_test", 50000000);
        System.out.println(System.currentTimeMillis());
        if (redisLock.tryLock(5000)) {
            System.out.println("2222222");
            // redisLock.unlock();
        }
        System.out.println(System.currentTimeMillis());
        log.info("test1 start");
        // testService.test();
        log.info("test1 end");
    }
}
