package cn.xxblog.web_demo.service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author barryp
 * @create 2018-07-06 10:50
 *     description:
 */
@Slf4j
@Service
public class TestServiceImpl implements TestService {

    @Override
    public synchronized Boolean test() {
        log.info("entry method.");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("quit method.");
        return Boolean.TRUE;
    }

}
