package cn.xxblog.cache.cn.xxblog.cache.util;

import lombok.extern.slf4j.Slf4j;

/**
 * Cache对象（反）序列化，装入可实例化的实体CacheSerializable
 * Created by Administrator on 16-11-25.
 */
@Slf4j
public class ObjectSerializer {
    /**
     * 使用Json序列化
     *
     * @param object
     * @return
     */
    public static byte[] serialize(Object object) {
        CacheSerializable obj = new CacheSerializable(object);

        String val = Converter.toJson(obj, false, false);
        return Converter.toUTF8(val);
    }

    /**
     * 反序列化对象：Serializable或json对象
     *
     * @param bytes
     * @return
     */
    public static <T> T unSerialize(byte[] bytes) {
        if (bytes == null) {
            return null;
        }

        if (bytes[0] == '[' || bytes[0] == '{') {
            //            log.debug("unSerialize: of JsonClassSerializer ");
            CacheSerializable<T> obj = Converter.parseObject(bytes, CacheSerializable.class);
            return obj.getObject();
        } else {
            return (T) Converter.unSerializeObject(bytes);
        }
    }

}
