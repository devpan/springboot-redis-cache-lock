package cn.xxblog.cache.cn.xxblog.cache.redis;

import cn.xxblog.cache.cn.xxblog.cache.Cache;
import cn.xxblog.cache.cn.xxblog.cache.CacheOperaterType;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

/**
 * 基于redis的缓存实现方案
 *
 * Created by devpan
 */
@Slf4j
public class RedisCache<T> extends AbstractRedisCache<T> implements Cache {

    public RedisCache() {
        this(null);
    }

    public RedisCache(String poolName) {
        super(poolName);
    }

    @Override
    protected Object doGet0(Jedis jedis, byte[] key, CacheOperaterType type) throws Exception {
        return null;

    }

    @Override
    protected void doPut0(Jedis jedis, byte[] key, byte[] value, CacheOperaterType type) throws Exception {

    }


}
