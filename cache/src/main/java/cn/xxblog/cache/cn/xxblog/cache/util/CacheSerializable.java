package cn.xxblog.cache.cn.xxblog.cache.util;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 可使用Json的多态序列化对象
 * Created by Administrator on 16-11-25.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CacheSerializable<T> {
    @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "@class")
    T object;
}
