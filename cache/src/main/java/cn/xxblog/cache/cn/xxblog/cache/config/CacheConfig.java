package cn.xxblog.cache.cn.xxblog.cache.config;

import cn.xxblog.cache.cn.xxblog.cache.CachePoolManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * redis cache config
 * @author barryp
 */
@Configuration
@Slf4j
@Order(-998)
public class CacheConfig implements ApplicationContextAware {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private int port;

    @Value("${spring.redis.timeout}")
    private int timeout;

    @Value("${spring.redis.jedis.pool.max-idle}")
    private int maxIdle;

    @Value("${spring.redis.jedis.pool.max-wait}")
    private long maxWaitMillis;

    @Value("${spring.redis.password}")
    private String password;


    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext appContext) throws BeansException {
        applicationContext = appContext;
    }


    public static Object getBean(String key) {
        return applicationContext == null ? null : applicationContext.getBean(key);
    }


    @Bean
    @ConditionalOnMissingBean
    public CacheConfig cacheConfig() {
        return new CacheConfig();
    }

    /**
     *
     * @return
     */
    @Bean(name = {"cachePoolManager"})
    @Order(1)
    public CachePoolManager createJedisPool() {
        log.info("注入 CachePoolManager 配置多Redis（JedisPool）连接池！！！");
        CachePoolManager poolManager = new CachePoolManager();

        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(maxIdle);
        jedisPoolConfig.setMaxWaitMillis(maxWaitMillis);
        // no auth ,if need you can add password param
        JedisPool jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout);
        poolManager.put("default", jedisPool);


        return poolManager;
    }

}
